#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <time.h>
#include <unistd.h>


#define SNAKE_BODY '+'
#define SNAKE_HEAD 'O'
#define FOOD '#'

#define UP 2
#define DOWN 3
#define LEFT 0
#define RIGHT 1

#define FOOD_TIMEOUT 1


void mutateSnake(int snake[][2], int length, int newHead[2], int grow) {
    int temp[2];
    temp[0] = snake[0][0];
    temp[1] = snake[0][1];
    snake[0][0] = newHead[0];
    snake[0][1] = newHead[1];

    for (int i = 1; i<length; i++) {
        int t[2];
        t[0] = snake[i][0];
        t[1] = snake[i][1];
        snake[i][0] = temp[0];
        snake[i][1] = temp[1];
        temp[0] = t[0];
        temp[1] = t[1];
    }

    if (grow) {
        snake[length + 1][0] = temp[0];
        snake[length + 1][0] = temp[1];
    }
}

void recalculateSnake(int snake[][2], int length, int pos, int grow) {
    int x = snake[0][0];
    int y = snake[0][1];
    int newHead[2];
    if (pos == 0) {         //this is left
        newHead[0] = x;
        newHead[1] = y - 2;
    }
    if (pos == 1) {         //this is right
        newHead[0] = x;
        newHead[1] = y + 2;
    }
    if (pos == 2) {         //this is up
        newHead[0] = x - 1;
        newHead[1] = y;
    }
    if (pos == 3) {         //this is down
        newHead[0] = x + 1;
        newHead[1] = y;
    }
    mutateSnake(snake, length, newHead, grow);
}

int isPointInSnake(int snake[][2], int length, int point[2]) {
    for (int i=0; i< length; i++) {
        if (snake[i][0] == point[0] && snake[i][1] == point[1]) {
            return 1;
        }
    }
    return 0;
}

void generateFood(int snake[][2], int length, int food[2]) {
    int x = rand() % LINES;
    int y = rand() % COLS;
    int point[2] = {x, y};
    while (isPointInSnake(snake, length, point) || y % 2 == 1 || x < 2) {
        x = rand() % LINES;
        y = rand() % COLS;
        point[0] = x;
        point[1] = y;
    }
    food[0] = point[0];
    food[1] = point[1];
}

int isSnakeEatingItself(int snake[][2], int length) {
    for (int i=1; i< length; i++) {
        if (snake[i][0] == snake[0][0] && snake[i][1] == snake[0][1]) {
            return 1;
        }
    }
    return 0;
}

int isSnakeBeyondEdge(int snake[][2]) {
    int x = snake[0][0];
    int y = snake[0][1];

    if (x < 2 || x > LINES - 1 || y < 0 || y > COLS - 1) {
        return 1;
    }
    return 0;
}

void resetSnakePositionAndLength(int snake[][2]) {
    snake[0][0] = LINES / 2;
    snake[0][1] = COLS / 2;
    snake[1][0] = snake[0][0] - 1;
    snake[1][1] = snake[0][1];
    snake[2][0] = snake[0][0] - 2;
    snake[2][1] = snake[0][1];
}

//graphics part

void printSnake(int snake[1000][2], int length) {
    mvprintw(snake[0][0], snake[0][1], "%c", SNAKE_HEAD);
    for (int i=1; i<length; i++) {
        mvprintw(snake[i][0], snake[i][1], "%c", SNAKE_BODY);
    }
}

void printFood(int food[]) {
    mvprintw(food[0], food[1], "%c", FOOD);
}

void printUpperLine() {
    for (int i=0; i<COLS; i++) {
        mvprintw(1, i, "-");
    }
}

void printScore(int score) {
    mvprintw(0, 0, "SCORE: %d", score);
}

void printLives(int lives) {
    mvprintw(0, COLS - 9, "LIVES: %d", lives);
}

void printMessage(char s[]) {
    attron(COLOR_PAIR(4));
    mvprintw(0, (int)(COLS / 2) - (int)(strlen(s) / 2), "%s", s);
}

void printContent(int snake[][2], int food[2], int score, int length, int lives, int boostEaten, int boost[2]) {
    attron(COLOR_PAIR(3));
    printScore(score);
    printLives(lives);
    printUpperLine();
    attron(COLOR_PAIR(1));
    printSnake(snake, length);
    attron(COLOR_PAIR(2));
    printFood(food);
    attron(COLOR_PAIR(5));
    if(!boostEaten) printFood(boost);
}

void printFinalMessage(int x, int y, char s[]) {
    attron(COLOR_PAIR(4));
    mvprintw(x, y, "%s", s);
}

void blinkSnake(int snake[][2], int food[], int score, int length, int lives, int boostEaten, int boost[2]) {
    for (int i=0; i<10; i++) {
        clear();
        //printing new content
        attron(COLOR_PAIR(3));
        printScore(score);
        printLives(lives);
        printUpperLine();
        attron(COLOR_PAIR(1));
        if (i % 2 == 0) printSnake(snake, length);
        attron(COLOR_PAIR(2));
        printFood(food);
        attron(COLOR_PAIR(5));
        if(!boostEaten) printFood(boost);
        //refreshing the content
        refresh();
        usleep(5 * 100000);
    }
}

int main(int argc, const char *argv[]){
    for (int argi = 1; argi < argc; argi++)
    {
        if (strcmp(argv[argi], "--debug-in-terminal") == 0)
        {
            printf("Debugging in terminal enabled\n");
            getchar(); // Without this call debugging will be skipped
            break;
        }
    }

    //variables
    int length = 3;
    int snake[1000][2];
    int food[2] = {10, 10};
    int boost[2] = {10, 10};
    int foodEaten = 0;
    long boostEatenTime = time(NULL);
    int boostEaten = 0;
    int growSnake = 0;
    int score = 0;
    int lives = 3;
    int pos = DOWN;
    int sleepTime = 1
    * 100000;
    int boostTimeTrashold = 10;
    long initTime = time(NULL);
    int speedTreshold = 10;

    //ncurses inicialization
    srand((int)time(NULL));
    initscr();
    cbreak();
    noecho();
    curs_set(FALSE);
    nodelay(stdscr, TRUE);
    keypad(stdscr,TRUE);
    start_color();


    //color pairs inicialization
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_WHITE, COLOR_BLACK);
    init_pair(4, COLOR_CYAN, COLOR_BLACK);
    init_pair(5, COLOR_YELLOW, COLOR_BLACK);

    generateFood(snake, length, boost);
    generateFood(snake, length, food);
    
    resetSnakePositionAndLength(snake);

    char x;
    clear();
    printMessage("Press space to start game");
    printContent(snake, food, score, length, lives, boostEaten, boost);
    refresh();
    while((x = getchar()) != 32) {}

    while (lives > 0) {
        int c;
        if ((c = getch())) {
            if (c == KEY_UP) {  // this is up
                pos = pos != DOWN ? UP : DOWN;
            }
            if (c == KEY_DOWN) {  // this is down
                pos = pos != UP ? DOWN : UP;
            }
            if (c == KEY_RIGHT) {  // this is right
                pos = pos != LEFT ? RIGHT : LEFT;
            }
            if (c == KEY_LEFT) {  // this is left
                pos = pos != RIGHT ? LEFT : RIGHT;
            }
            if (c == 32) {
                char x;
                clear();
                printMessage("Press space to reusme");
                printContent(snake, food, score, length, lives, boostEaten, boost);
                refresh();
                while((x = getchar()) != 32) {}
            }
        }

        //reduce delay every 10 secont by 5%
        if (difftime(time(NULL), initTime) > speedTreshold) {
            initTime = time(NULL);
            sleepTime = (int)sleepTime * 0.95;
        }

        //recalculate snake array
        recalculateSnake(snake, length, pos, growSnake);

        // check if snake is beyond enge
        if(isSnakeBeyondEdge(snake)) {
            blinkSnake(snake, food, score, length, lives, boostEaten, boost);
            lives = lives - 1;
            resetSnakePositionAndLength(snake);
            length = 3;
            pos = DOWN;
        }

        // check if is snake eating himself
        if (isSnakeEatingItself(snake, length)) {
            blinkSnake(snake, food, score, length, lives, boostEaten, boost);
            lives = lives - 1;
            resetSnakePositionAndLength(snake);
            length = 3;
            pos = DOWN;
        }

        //if snake ate food last iteration then grow
        if (growSnake) {
            growSnake = 0;
            length++;
        }

        // check if snake is eating in current iteration
        if (isPointInSnake(snake, length, food)) {
            foodEaten = 1;
            growSnake = 1;
            score++;
        }
        
        if (isPointInSnake(snake, length, boost)) {
            boostEatenTime = time(NULL);
            boostEaten = 1;
        }

        // if food was eaten, then generate new food
        if (foodEaten) {
            generateFood(snake, length, food);
            foodEaten = 0;
        }
        
        if (boostEaten && difftime(time(NULL), boostEatenTime) > boostTimeTrashold) {
            generateFood(snake, length, boost);
            boostEaten = 0;
        }

        //clearing screen
        clear();

        //printing new content
        attron(COLOR_PAIR(3));
        printScore(score);
        printLives(lives);
        printUpperLine();
        attron(COLOR_PAIR(1));
        printSnake(snake, length);
        attron(COLOR_PAIR(2));
        printFood(food);
        attron(COLOR_PAIR(5));
        if(!boostEaten) printFood(boost);
        //refreshing the content
        refresh();
        usleep(boostEaten && difftime(time(NULL), boostEatenTime) < boostTimeTrashold / 2 ? 2 * 100000 : sleepTime);
    }

    clear();
    char a[50];
    sprintf(a, "END OF THE GAME, YOUR SCORE: %d", score);
    printFinalMessage(LINES / 2 - 2, (int)(COLS / 2) - (int)(strlen(a) / 2), a);
    printFinalMessage(LINES / 2 - 1, (int)(COLS / 2) - (int)(strlen("press any key for ending the game") / 2), "press any key for ending the game");
    refresh();
    while(! getchar()) {}
    endwin();
}
